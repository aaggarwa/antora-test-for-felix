# Antora Test for FELIX

A test setup to incorporate Antora within FELIX repos.

git clone this repo and do:

antora antora-playbook.yml

Antora will clone the content repositories. The cloning progress of each repository is displayed in the terminal.
Then go to build/site/ and do "open index.html".This will open the site directly from the command line.
